import os
import unittest
import json
from app import app, db
from app.models import MalwareLocation
from utils.data_import import get_urls_from_txt_file, add_urls_to_db
from config import basedir

class ImportListFromFileTestCase(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        db.create_all()
        self.app = app.test_client()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_add_urls_to_db(self):
        urls = get_urls_from_txt_file(os.path.join(basedir, 'zeus_compromised_sample.txt'))
        add_urls_to_db(urls)
        m = MalwareLocation.query.count()
        r =  json.loads(self.app.get('/adf.com.cy/admin/gate.php').data)
        u = len(urls)

        self.assertEqual(u,m)


if __name__ == '__main__':
    unittest.main()