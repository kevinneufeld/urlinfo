import os
import unittest
from utils.data_import import get_urls_from_txt_file
from utils.validators import is_valid_url
from config import basedir

class TestUntils(unittest.TestCase):

    def test_url_regex(self):
        self.assertTrue(is_valid_url('https://text.com/this_is_a_test/q?test=true'))
        self.assertFalse(is_valid_url('test'))

    def test_get_models_from_file(self):
        urls = get_urls_from_txt_file(os.path.join(basedir, 'zeus_compromised_sample.txt'))
        self.assertIsNotNone(urls)
