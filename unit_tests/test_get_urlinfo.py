import os
import unittest
import json
from app import app, db
from app.models import MalwareLocation
from utils.data_import import get_urls_from_txt_file, add_urls_to_db
from config import basedir


def _init_database():
    m = MalwareLocation(url_pattern='testurl.com/this_is_a_test/q?test=true')
    db.session.add(m)
    db.session.commit()
    return m

class GetUrlInfoTestCase(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        db.create_all()
        _init_database()
        self.app = app.test_client()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_add_malwareurl(self):
        r = MalwareLocation.query.count()
        self.assertTrue(r == 1)

    def test_is_url_blocked_true(self):
        r = json.loads(self.app.get('/testurl.com/this_is_a_test/q?test=true').data)
        self.assertTrue(r['block'])

    def test_is_url_blocked_false(self):
        r = json.loads(self.app.get('/testurl.com/this_is_a_test/q?test=false').data)
        self.assertFalse(r['block'])

if __name__ == '__main__':
    unittest.main()