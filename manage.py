from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from utils.data_import import get_urls_from_txt_file, add_urls_to_db
import os

from app import app, db

app.config.from_object('config')

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

@manager.option('-f', '--file_path', dest='file_path')
def import_from_txt_file(file_path):

    if os.path.isfile(file_path):
        add_urls_to_db(get_urls_from_txt_file(file_path))
    else:
       print "File does not exist"

if __name__ == '__main__':
    manager.run()