from flask import request, jsonify
from app import app
from app.models import MalwareLocation


def is_url_blocked(req):
    #'Strip off scheme and application root to get maleware url'
    pattern = req.url[len(req.host_url + req.script_root[1:] + "/"):]

    # Query for pattern'
    result = MalwareLocation.query.filter_by(url_pattern=pattern).first()
    return {"block": (False if result is None else True)}

@app.route('/<path:path>')
def urlinfo(path):
    return jsonify(is_url_blocked(request))
