from app import db
from datetime import datetime
# Define a base model
class Base(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())
#Define MalwareLocation
class MalwareLocation(Base):
    __tablename__ = 'blocklist'
    __table_args__ = {'sqlite_autoincrement': True}
    url_pattern = db.Column(db.Text, )

    def __init__(self, url_pattern):
        self.url_pattern = url_pattern

    def __repr__(self):
        return '<MalwareLocation %r>' % (self.url_pattern)
