import os
basedir = os.path.abspath(os.path.dirname(__file__))

# Set application
APPLICATION_ROOT = '/urlinfo/1'
JSONIFY_PRETTYPRINT_REGULAR = False

# Define database
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'blocked_urls.db')
SQLALCHEMY_TRACK_MODIFICATIONS = False