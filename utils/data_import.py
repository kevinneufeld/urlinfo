

def add_urls_to_db(urls):
    from app import db
    from app.models import MalwareLocation

    for p in urls:
        db.session.add(MalwareLocation(url_pattern=p))

    db.session.commit()

def get_urls_from_txt_file(file_path):
    from validators import is_valid_url
    from app.models import MalwareLocation
    urls = []

    try:
        with open(file_path) as f:
            lines = f.readlines()

        for l in lines:
            if is_valid_url(l.rstrip('\n')):
                urls.append(l.rstrip('\n'))

    except IOError as e:
        print "Unable to open file."

    return urls


