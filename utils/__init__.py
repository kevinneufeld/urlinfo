from flask_script import Manager

from app import app, db
app.config.from_object('config')

from app.models import MalwareLocation

manager = Manager(app)
