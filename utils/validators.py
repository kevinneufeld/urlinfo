
def get_url_regex_pattern():
    import re
    regex = re.compile(
            r'(?P<scheme>^https?://)?'  # http:// or https://
            r'(?P<host>(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}))'  # ...or ip
            r'(?P<port>(?::\d+))?'  # optional port
            r'(?P<path>(?:/?|[/?]\S+))$', re.IGNORECASE)
    return regex


def is_valid_url(url):
    import re
    regex = re.compile(
        r'(?P<scheme>^https?://)?'  # http:// or https://
        r'(?P<host>(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}))'  # ...or ip
        r'(?P<port>(?::\d+))?'  # optional port
        r'(?P<path>(?:/?|[/?]\S+))$', re.IGNORECASE)
    return url is not None and regex.search(url)

